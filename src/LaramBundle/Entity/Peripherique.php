<?php

namespace LaramBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Peripherique
 *
 * @ORM\Table(name="peripherique", indexes={@ORM\Index(name="fk_avion_peripherique", columns={"avion_id"})})
 * @ORM\Entity(repositoryClass="LaramBundle\Repository\PeripheriqueRepository")
 */
class Peripherique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;


    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="LaramBundle\Entity\Avion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="avion_id", referencedColumnName="id")
     * })
     */
    private $avion;

    /**
     * @param integer $avion
     * 
     * @return Avion
     */
    public function setAvion($avion)
    {
        $this->avion = $avion;

        return $this;
    }

    /**
     * Get avion
     * 
     * @return int
     */
    public function getAvion()
    {
        return $this->avion;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Peripherique
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
}
