<?php

namespace LaramBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Library
 *
 * @ORM\Table(name="library", indexes={@ORM\Index(name="fk_peripherique_library", columns={"peripherique_id"})})
 * @ORM\Entity(repositoryClass="LaramBundle\Repository\LibraryRepository")
 */
class Library
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ERD", type="string", length=255)
     */
    private $eRD;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="location_id", type="string", length=255)
     */
    private $locationId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;
    
    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="LaramBundle\Entity\Peripherique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="peripherique_id", referencedColumnName="id")
     * })
     */
    private $peripherique;

    /**
     * @param integer $peripherique
     * 
     * @return Peripherique
     */
    public function setPeripherique($peripherique)
    {
        $this->peripherique = $peripherique;
        
        return $this;
    }

    /**
     * Get peripherique
     * 
     * @return int
     */
    public function getPeripherique()
    {
        return $this->peripherique;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eRD.
     *
     * @param string $eRD
     *
     * @return Library
     */
    public function setERD($eRD)
    {
        $this->eRD = $eRD;

        return $this;
    }

    /**
     * Get eRD.
     *
     * @return string
     */
    public function getERD()
    {
        return $this->eRD;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Library
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locationId.
     *
     * @param string $locationId
     *
     * @return Library
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId.
     *
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return Library
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Library
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
