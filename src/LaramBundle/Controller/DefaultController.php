<?php

namespace LaramBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Entity\Library;
use Entity\Avion;
use Entity\Peripherique;

class DefaultController extends Controller
{
    /**
     * @Route("/about", name="about_us")
     */
    public function aboutAction(Request $request)
    {
        $result = $request->query->getInt('me',1);
        
        return $this->render('default/about.html.twig',
        [
            "data"=> $result
        ]);
    }


    /**
     * @Route("/main", name="main")
     */
    public function mainAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository("LaramBundle:Library")->findAll();
        //findBy(['peripherique_id' => ':id']);
        $avion = $em->getRepository("LaramBundle:Avion")->findAll();
        $peripherique = $em->getRepository("LaramBundle:Peripherique")->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',10)
        );
        
        return $this->render('default/main.html.twig',
        [
            "data"=> $result,
            "avion"=> $avion,
            "peripherique"=> $peripherique
        ]);
    }

    /**
     * @Route("/library/{id}", name="library")
     */
    public function libraryAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT lib FROM LaramBundle:Library lib WHERE lib.peripherique=:id")
        ->setParameter('id',intval($id));
        //getRepository("LaramBundle:Library")->findAll();
        //findBy(['peripherique_id' => ':id']);
        $avion = $em->getRepository("LaramBundle:Avion")->findAll();
        $peripherique = $em->getRepository("LaramBundle:Peripherique")->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $query,
            $request->query->getInt('page',1),
            $request->query->getInt('limit',10)
        );
        
        return $this->render('default/main.html.twig',
        [
            "data"=> $result,
            "avion"=> $avion,
            "peripherique"=> $peripherique
        ]);
    }


}
